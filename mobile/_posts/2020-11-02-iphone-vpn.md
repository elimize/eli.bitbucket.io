---
layout: post
published: true
title: "[아이폰/iphone] VPN 이용하여 차단된 사이트 우회 접속 방법"
image: /assets/images/iphone.jpg
description: >
  아이폰에서 VPN을 이용해서 차단된 사이트에 우회 접속하는 방법에 대해서 소개해드립니다.
author: Eli
toc: true
permalink: /mobile/iphone-vpn
tags: 
  - 아이폰
  - VPN
  - iphone
  - 우회 접속
---

요즘들어 나라 자체에서 국민들의 자유를 침해하는 사례들이 많아지고 있습니다. 그 중에서도 이해가 되지 않는 것이 바로 웹사이트의 접근을 제한 하는 것입니다.

그 방법은 우리나라 통신사에게 개입하여 유해하다고 판단되는 사이트의 제한하는 방식(`https 차단`)을 취하고 있습니다. 이에 따라 폰허브(pornhub), 엑스비디오(xvideo) 등의 접근을 할 수가 없습니다.

그래서 이 번 글에서는 통신사에서 특정 웹사이트의 접근을 막는 방법에 대해서 소개해드리고 이 것을 우회하는 방법에 대해서 알아보도록 하겠습니다.

## 아이폰 https 차단 우회

우선 https 차단은 무엇인지 알아보고, 이 것을 우회할 수 있는 수단은 어떤 것들이 있는지 알아보도록 하겠습니다.

### https 차단

https가 탄생하기 전에는 모든 웹사이트는 http 프로토콜 기반으로 운영되고 있었습니다. http 프로토콜은 `암호화가 되지 않은 상태`이기 때문에 ISP 업체에서 특정 호스트 (ex. pornhub.com)에 대한 차단을 할 수 있었습니다.

보안이 강화된 https 프로토콜이 보편화되면서 호스트가 암호화되어 차단이 무력화되었습니다. 하지만 https라 하더라도 요청을 하기 전에 `Hand Shaking(네트워크 연결 검증)` 과정을 거치기 때문에 이 과정에서는 암호화가 적용되지 않은 상태입니다.

통신사 같은 ISP 업체는 이 정보(SNI)를 이용하여 호스트를 구분하여 차단을 하는 방식을 새롭게 적용하였습니다.

### 아이폰에서 우회 접속하는 방법

PC에서 웹으로 이용을 한다면 다양한 Extension 프로그램이나 터미널 조작으로 간단하게 해결할 수 있지만, 모바일 환경에서는 제한적인 경우가 많습니다.

그래서 보통 많은 사람들이 사용하는 우회접속을 하도록 해주는 어플리케이션을 소개드리려고 합니다.

{% include components/google/infeed-ad.html slot=site.data.ad.third.infeed %}

#### 유니콘 HTTPS 우회 앱

![download unicorn app](/assets/images/unicorn1.jpg)

아이폰 앱스토어에서 위와 같이 유니콘을 검색하고, **유니콘 HTTPS 우회 앱**을 다운로드 해줍니다. 다운로드한 앱을 실행하면 처음에 알림 허용 등의 설정을 하라고 알림이 뜹니다.

알림 설정 관련은 원하시는 대로 설정해주시면됩니다.

![run unicorn app](/assets/images/unicorn2.jpg)

앱을 실행하시고 첫 화면 가운데에 있는 토글 버튼을 클릭하면 위와 같이 VPN과 관련된 설정을 해주어야한다고 알림이 뜹니다. `허용` 버튼을 눌러 설정 앱으로 이동합니다.

![configure unicorn app](/assets/images/unicorn3.jpg)

VPN은 중요한 설정이므로 비밀번호를 요구합니다. 입력하신 후 오른쪽과 같이 **Unicorn HTTPS**과 관련된 설정이 되어 있다면 성공적으로 잘 동작할 것입니다.

![check unicorn app](/assets/images/unicorn4.jpg)

이제 앱으로 이동하여 토글이 정상적으로 활성화 되어 있는지 확인합니다.

또한 상단 바에 `VPN`이라고 써진 마킹이 떠있다면 정상적으로 실행중인 것을 확인할 수 있습니다.

## 마무리

이렇게 해서 아이폰에서 https 차단을 우회하는 앱에 대해서 소개해드렸습니다.

간단하게 앱을 이용해서 폰헙, 엑스비디오 등의 사이트를 접근 할 수 있습니다. 궁금하신 점이나 이상한 점이 있다면 댓글 부탁드리겠습니다.

감사합니다.
