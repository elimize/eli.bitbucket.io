---
layout: post
published: true
title: 아이폰 일련 번호 조회하는 방법
image: /assets/images/iphone.jpg
description: >
  아이폰 일련 번호 조회하는 방법에 대해서 소개해드리도록 하겠습니다.
author: navy apple
toc: true
permalink: /mobile/ihpone-serial
tags: 
  - 아이폰
  - 일련 번호
  - 시리얼
  - iphone
  - serial
---

폰을 바꿀 때, 아이폰의 경우 아이폰 **일련번호**가 필요한 경우가 간혹 있습니다. 그 이유는 아무래도 이전에 사용하던 폰이 아이폰을 경우, 기기의 인증을 하기 위함입니다.

이 글에서는 아이폰에서 `시리얼 번호`를 확인하는 방법에 대해서 소개해드리도록 하겠습니다.

## 아이폰 일련 번호 조회하는 방법

시리얼 번호를 조회하기 위해서는 설정 앱의 아이폰의 정보를 봐야합니다.

### 일반적인 조회 방법

다음과 같이 간단하게 조회가 가능합니다.

![settings](/assets/images/iphone-serial1.jpg)

> 1. `설정` 앱에서 `일반` 탭으로 들어갑니다.
> 2. `일반` > `정보` 탭으로 들어갑니다.

### 검색을 통해서 조회 하는 방법

또는 다음과 같이 `설정`앱 처음 화면에서 검색을 통해서 조회 할 수도 있습니다.

![search settings](/assets/images/iphone-serial2.jpg)

1. `설정` 앱 검색창에 `정보`라고 검색합니다.
2. `정보` 탭 내부에 `일련번호`를 확인하실 수 있습니다.

## 마무리

간단하게 아이폰에서 일련번호 또는 시리얼번호를 조회하는 방법에 대해서 소개해드렸습니다.

혹시 궁금하신 점이나 이상한 점이 있으면 댓글 부탁드리겠습니다.

감사합니다.
