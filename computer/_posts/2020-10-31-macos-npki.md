---
layout: post
published: true
title: "[맥/윈도우] 공인인증서 위치"
image: /assets/images/finder.jpg
description: >
  Mac OS 및 Windows 공인인증서 위치 및 브라우저에 공인인증서 가져오는 방법에 대해서 소개합니다.
author: Eli
toc: true
permalink: /computer/npki
tags:
  - macOS
  - 공인인증서
  - 인증서
  - 인증서 위치
---

어느 순간부터 은행권이나 정부기관들이 인터넷 상에서 유저를 인증하기 위한 수단으로 **공인인증서**라는 것을 사용하게 되었습니다.

확실히 인터넷 상에서 인증이 가능하면서 편해진 점이 많지만, 공인인증서를 사용하기 위해서는 약간 까다로운 과정이 필요합니다.

이 글에서는 맥에서 공인인증서가 어떤 폴더에 위치해 있는지 소개해드리는 시간을 가져보겠습니다.

## 공인인증서

공인인증서는 인터넷 상에서 자신임을 증명하기 위한 수단 중에 하나입니다.

공인인증서를 사용하기 위해서는 은행권이나 정부기관에서 본인인증을 통해서 공인인증서를 발급 받고, 자신의 `컴퓨터`나 브라우저에 저장하여야합니다.

요즘에는 브라우저 공인인증서가 생겨나면서 최신 브라우저에서 바로 이용가능하도록 간편화 되었지만 과거에는 은행권에서 공인인증서를 발급 받기 위해서는 수 많은 보안 플러그인이나 패키지를 설치했어야했습니다.

그런데 이런 플러그인이나 패키지가 특정 OS, 특정 브라우저에서만 동작하는 문제들이 있어서 많은 사람들이 불편함을 호소했습니다.

### 공인인증서 위치

위에서 공인인증서를 컴퓨터나 브라우저에 저장한다고 언급했었습니다. 컴퓨터로 저장하는 것은 컴퓨터의 하드디스크로 저장이 되고, 브라우저 인증서의 경우 보통 클라우드를 이용하는 경우가 많습니다.

그래서 브라우저는 따로 다루지 않고 컴퓨터에 저장한 경우, 윈도우와 맥에서 각각 어디에 저장되는지 알아보도록 하겠습니다.

{% include components/google/infeed-ad.html slot=site.data.ad.third.infeed %}

#### Windows OS

![c drive users](/assets/images/npki1.jpg)

우선, 내 PC > C드라이브 > 사용자 폴더로 이동합니다.

![users](/assets/images/npki2.jpg)

자신의 계정을 이름으로 하는 폴더로 이동합니다. 공인인증서의 경우 보안이 중요하기 때문에 숨겨진 폴더에 저장되어 있습니다.

따라서 왼쪽 상단의 `보기` 버튼을 클릭합니다.

![folder options](/assets/images/npki3.jpg)

`보기` 버튼을 클릭하면 위와 같이 다양한 폴더 옵션들을 설정할 수 있는 패널이 나옵니다.

여기서 오른쪽에서 세 번쩨 항목에 `숨긴 항목`을 체크해줍니다.

![hidden folder](/assets/images/npki4.jpg)

숨긴 항목에 체크를 하면 위와 같이 숨겨져 있던 폴더들이 보여지게 됩니다.

**AppData** 폴더에 공인인증서가 들어 있습니다.

![npki folder](/assets/images/npki5.jpg)

> AppData > LocalLow > NPKI

위 위치로 들어가시면 공인인증서를 확인하실 수 있습니다.

{% include components/hint-box.html type='info' text='C:\Users\[유저이름]\AppData\LocalLow\NPKI' %}

#### Mac OS

맥 OS의 경우도 Windows 비슷하게 다음과 숨겨져 있지만, 경로로 이동하는 것이 가장 빠릅니다.

![mac npki](/assets/images/mac-npki.jpg)

파인더를 켠 상태에서 **⌘ + ↑** 키를 누른 후 다음 경로를 입력해주면 됩니다.

{% include components/hint-box.html type='info' text='~/library/Preferences/NPKI' %}

## 마무리

이렇게 윈도우와 맥에서 각각 공인인증서의 위치를 확인해보는 시간을 가져봤습니다.

혹시 궁금하신 점이나 이상한 점이 있으시면 댓글 부탁드리겠습니다.

감사합니다.