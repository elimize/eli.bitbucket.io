---
layout: post
published: true
title: "[맥/Mac] 파인더 경로 이동하는 방법"
image: /assets/images/finder.jpg
description: >
  맥 파인더 앱에서 경로를 입력하여 바로 타겟 폴더로 이동하는 방법에 대해서 소개합니다.
author: Eli
toc: true
permalink: /computer/move-path-in-mac-finder
tags:
  - macOS
  - finder
  - path
  - 파인더 경로
  - 폴더 경로
---

요즘에는 한국에서 맥 사용자가 많이 늘어나고 있는 것을 느낍니다. 하지만 한국의 대부분의 사람들이 IBM 기반의 Windows OS에 익숙해져 있습니다.

그래서 처음 디자인이 예쁘다는 이유로 구매했던 맥에 Windows OS를 설치해서 쓰는 사람들도 있기도 해서 안타까운 생각이 들기도합니다.

그래서 이번 포스티에서는 Windows에서 탐색기에 해당하는 MacOS의 **파인더 (finder)**라는 앱에 대해서 소개해드리도록 하겠습니다.

## 파인더란?

파인더란 Windows에서 탐색기에 해당하는 MacOS에서의 파일 시스템을 보거나 편집하기 쉽도록 도와주는 기능을 하는 어플리케이션입니다.

| ![windows explorer](/assets/images/explorer1.jpg) | ![mac finder](/assets/images/finder-path1.jpg) |
|:-:|:-:|
| Windows | Mac OS |

## 파인더 기능

### 상위 폴더로 이동 하는 방법

| ![windows explorer](/assets/images/explorer3.jpg) | ![windows explorer path input](/assets/images/finder-path1.jpg) |
|:-:|:-:|
| Windows | Mac OS |

윈도우즈에서는 왼쪽 상단의 ↑ 버튼을 누르면거나 **Alt + ↑** 버튼을 눌러 상위 폴더로 이동할 수 있습니다.

맥에서는 상위 폴더로 이동하는 버튼은 없고, 단축키 **⌘ + ↑** 버튼을 눌러 이동이 가능합니다.

### 경로 입력해서 이동 하는 방법

```bash
/
├── /Users
│   ├── /Users/Downloads
│   │   ├── /Users/Downloads/Secrets
│   │   │   └── /Users/Downloads/Secrets/Somethings
```

위와 같이 파일 시스템에서 폴더 안의 폴더의 방식으로 depth가 깊어지게 만들 수 있습니다.

이렇게 depth가 깊은 폴더가 있다면, 목표로하는 폴더로 이동하는 것에 시간이 걸릴 수도 있고 찾지 못하는 경우도 간혹 발생하곤 합니다.

이러한 경우에 사용하는 방법이 경로를 메모에 저장해두었다가 직접 이동할 수 있는 방법이 있습니다.

{% include components/google/infeed-ad.html slot=site.data.ad.third.infeed %}

#### Windows

| ![windows explorer](/assets/images/explorer1.jpg) | ![windows explorer path input](/assets/images/explorer2.jpg) |
|:-:|:-:|
| 파인더 | 파인더 경로 입력창 |

윈도우즈에는 탐색기 상단에 바로 경로를 입력할 수 있는 입력 공간이 있기 때문에 입력 공간에 경로를 입력하여 이동합니다.

#### Mac

| ![mac finder](/assets/images/finder-path1.jpg) | ![mac finder path input](/assets/images/finder-path2.jpg) |
|:-:|:-:|
| 파인더 | 파인더 경로 입력창 |

맥의 경우는 파인더 기본 화면에 경로 입력 공간이 따로 없습니다. 따라서 다음과 같이 하면 입력 공간이 나타나게 됩니다.

{% include components/hint-box.html type='info' list='⌘ + shift + g 입력|입력 공간에 원하는 경로를 입력' %}

## 마무리

간단하게 맥에서 상위폴더로 이동하는 방법과 경로를 입력하여 이동하는 방법에 대해서 알아보았습니다.

혹시 궁금하신 점이나 이상한 점이 있으시면 댓글 부탁드리겠습니다.

감사합니다.
