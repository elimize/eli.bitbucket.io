---
layout: post
published: true
title: "[맥/Mac] 노트 자동완성 끄기 (비활성)"
image: /assets/images/mac-notes.jpg
description: >
  MacOS 노트 앱의 자동완성 기능을 비활성 하는 방법에 대해서 소개합니다.
author: Eli
toc: true
permalink: /computer/disable-auto-complete-in-mac
tags:
  - macOS
  - 맥OS
  - 노트
  - 자동완성 끄기
  - 자동완성 비활성
---

요즘들어 맥을 사용하는 유저들이 많아지고 있는 것 같습니다. 하지만 맥을 처음 접하거나 익숙하지 않은 사용자들에게는 사용하기 어려운 것이 현실입니다.

이번 글에서는 맥에서 기본으로 제공하는 **노트(Notes)** 앱에서 글을 썼을 때, 자동완성 기능을 비활성화하는 방법에 대해서 알아보도록 하겠습니다.

## 노트 (Notes)

노트 앱에서는 자동으로 고처주는 것을 `Smart Substitutions`이라고 표현하는 자동으로 글을 고쳐주는 기능을 가지고 있습니다.

하지만 다양한 기능들은 유용할 수도 있지만, 사용하는 사람에 따라서는 불편하게 느껴질 수 있습니다. 아래에는 스마트 기능에는 무엇이 있는지 알아보고 각 기능을 비활성 했을 때 어떻게 적용되는지 확인해보도록 하겠습니다.

### 스마트 치환

![smart substitutions summary](/assets/images/smart-substitutions-summary.jpg)

위 사진에 자주 쓰일만한 기능들의 활성과 비활성 상태에서 예제를 만들어 요약해 놓았습니다. 이제는 각 기능에 대해서 알아보도록 하겠습니다.

#### 스마트 따옴표

이 기능은 간혹 성가실 수 있는 기능입니다. 기본적으로 맥 노트 앱에서는 다음과 같이 자동으로 따옴표를 변환해줍니다.

```
따옴표
' -> ‘ or ’

쌍 따옴표
" -> “ or ”
```

#### 스마트 링크

이 기능은 링크로 판단되는 문자열이 발견되면 자동으로 클릭하여 이동가능한 링크로 만들어줍니다.

#### 스마트 리스트

이 기능은 나름 유용하게 사용할 수 있는 기능으로 `-`, `*`, `숫자` 등을 사용하여 간단하게 리스트를 만들 수 있도록 해줍니다.

#### 스마트 대시

이 기능은 알고 계시는 분들이 많지 않을 것이라고 생각하는 기능입니다. `-`를 여러 개 사용하면 자동으로 이어진 실선을 만들어줍니다.

```bash
-----------------  ->  —————————————————
```

{% include components/google/infeed-ad.html slot=site.data.ad.third.infeed %}

### 스마트 치환 기능 비활성화 하는 방법

스마트 치환 기능을 비활성화하는 방법은 간단합니다.

![config smart substitutions](/assets/images/config-smart-substitutions.jpg)

{% include components/hint-box.html type='info' list='Edit > Substitutions > Smart *|비활성화 하려는 기능을 클릭' %}

## 마무리

이번 글에서는 맥에서 자동으로 글을 고쳐주는 기능을 해제하는 방법에 대해서 알아보았습니다. 혹시 궁금하신 점이나 이상한 점이 있다면 댓글 부탁드리겠습니다.

감사합니다.
