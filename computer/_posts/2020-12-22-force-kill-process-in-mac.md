---
layout: post
published: true
title: "[맥/Mac] 앱 강제 종료하는 방법"
image: /assets/images/mac-force-quit.jpg
description: >
  MacOS에서 앱을 강제 종료하는 방법을 소개해드리도록 하겠습니다.
author: Eli
toc: true
permalink: /computer/force-kill-process-in-mac
tags:
  - macOS
  - 맥OS
  - 앱 강제 종료
  - 강제 종료
---

맥에서 앱이 정상적으로 동작하지 않거나 응답을 하지 않는 경우 아무리 종료 버튼을 클릭해도 앱이 종료되지 않는 경우가 있습니다.

이런 경우, 앱을 강제 종료하여 재실행을 해야합니다. 이번 글에서는 맥에서 앱을 강제 종료 시키는 방법에 대해서 소개해드리도록 하겠습니다.

## 앱 강제 종료하는 방법

맥에서 실행중인 앱을 강제 종료하는 방법은 두 가지가 있습니다.

> 강제 종료 방법
> - Force Quit Applications에서 강제 앱 강제 종료
> - Activity Monitor에서 앱 강제 종료

첫 번째 방법은 굉장히 간단하게 실행중이거나 응답이 없는 앱을 강제 종료 할 수 있고, 만약에 상태바를 통해서 들어간 `Force Quit Applications`에서 종료 시킬 앱을 찾을 수 없으면 두 번째 방법을 이용해야합니다.

두 번째 방법은 모든 작동중인 프로세스를 볼 수 있는 Activity Monitor에서 앱을 찾아서 강제 종료하는 것입니다.

### Force Quit Applications 강제 종료

이 방법은 가장 간단하게 앱을 강제 종료할 수 있는 방법입니다. 아래와 같이 따라해주시면 간단하게 앱을 강제 종료 할 수 있습니다.

![상태바](/assets/images/force-kill-app1.jpg)

위 사진 처럼 상단의 상태바에서 애플 아이콘을 클릭하고, `force quit`으로 시작하는 메뉴를 클릭합니다.

![어플리케이션 강제 종료](/assets/images/force-kill-app2.jpg)

위 사진처럼 강제 종료하고 싶은 앱을 선택하고, `Force Quit` 버튼을 클릭하여 바로 앱을 강제 종료 할 수 있습니다.

또한 강제 종료 어플리케이션을 바로 실행할 수 있는 단축키는 `⌘` + `⌥` + `esc` 입니다.

{% include components/google/infeed-ad.html slot=site.data.ad.third.infeed %}

### Activity Monitor에서 강제 종료

만약에 `Force Quit Applications`에서 앱을 찾을 수 없는 경우는 아래 방법을 따라해주시면 간단하게 원하는 앱을 강제 종료시킬 수 있습니다.

![Activity Monitor 실행](/assets/images/force-kill-app3.jpg)

하일라이트에서 `Activity Monitor`를 검색하고, 실행해줍니다.

![Activity Monitor 윈도우](/assets/images/force-kill-app4.jpg)

Activity Monitor를 실행하면 위와 같이 모든 프로세스 리스트를 한 눈에 볼 수 있습니다.

강제로 종료시키고 싶은 앱을 선택하신 후, 왼쪽 상단의 `X` 버튼을 클릭합니다.

![Activity Monitor에서 강제 종료](/assets/images/force-kill-app5.jpg)

`X` 버튼을 클릭하면 해당 앱을 종료하겠다는 알림 창이 뜨고 하단 버튼 중에서 `force Quit` 버튼을 눌러서 앱을 강제로 종료할 수 있습니다.

## 맺음

이 번 글에서는 맥에서 앱을 강제 종료 시키는 방법에 대해서 알아보았습니다. 혹시 궁금하신 점이나 이상한 점이 있다면 댓글 부탁드리겠습니다.

감사합니다.
