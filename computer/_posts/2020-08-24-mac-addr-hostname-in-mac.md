---
layout: post
published: false
title: "[macOS] 맥북에서 맥 어드레스, 호스트 네임 확인 (Check your MAC address and hostname in macOS)"
image: /assets/img/blog/mac-system-prefer.png
description: >
  [macOS] 맥북에서 맥 어드레스, 호스트 네임 확인 (Check your MAC address and hostname in macOS)
author: navy apple
comments: true
permalink: /tips/life/mac-addr-hostname-in-mac
tags: 
  - macOS
  - 맥북
  - 맥 주소
  - mac address
  - hostname
---

## [macOS] 맥북에서 맥 어드레스, 호스트 네임 확인 (Check your MAC address and hostname in macOS)

다양한 이유로 맥북의 `맥 주소 (mac address)` 또는 `호스트 네임 (hostname, PC 이름)`을 찾아야 하는 경우가
있습니다.

이 번에는 맥에서 `맥 주소`와 `호스트 네임 (PC 이름)`을 확인하는 방법에 대해서 알아보도록 하겠습니다.

### 맥 주소 확인하기 (Check mac address)

![spotlight](/assets/img/blog/mac-add1.png)

* `스포트라이트(Spotlight)`(`⌘` + `Space`)를 열고 `시스템 설정(System Preferences)`를 검색합니다.

![system_preferences](/assets/img/blog/mac-add2.png)

* 시스템 설정에 여러가지 카테고리 중 `네트워크(Network)`를 선택합니다.

![network](/assets/img/blog/mac-add3.png)

* 네트워크 카테고리에서 `고급(advanced..)`를 클릭합니다.

![advanced](/assets/img/blog/mac-add4.png)

* 고급 설정화면 서브 탭들 중 `Hardware` 탭을 클릭하면 `맥 주소(mac address)`를 확인 할 수 있습니다.

#### **정리**

1. `스포트라이트(Spotlight)`로 `시스템 설정(System Preferences)` 열기
2. `시스템 설정(System Preferences)`의 `네트워크(Network)` 열기
3. `네트워크(Network)` 카테고리의 고급 열기
4. `맥 주소(mac address)` 확인

<hr>

### 호스트 네임 또는 PC 이름 확인 하기(Check hostname or PC name)

![system_preferences2](/assets/img/blog/mac-add5.png)

* 시스템 설정 카테고리 중 `공유(Sharing)`를 선택합니다.

![sharing](/assets/img/blog/mac-add6.png)

* 공유 카테고리에서 `computer name`이 이 컴퓨터의 이름입니다.


<hr>

### 마무리

이렇게 간단하가게 맥OS에서 맥주소(mac address) 확인하는 법과 호스트 이름(hostname) 또는 PC 이름(PC name) 확인하는 벙법에 대해서
알아보았습니다. 

제가 예상했던 방법은 터미널 들어가서 `ifconfig` 등등 명령어를 실행하여 확인하는 방법을 예상했지만 이렇게 간단하게
`시스템 설정(System Preferences)`에서 바로 확인할 수 있다는 점이 좋았습니다.

이런식으로 확인할 수 있다면, 어린아이들도 기억하기 쉬울 것 같네요.

감사합니다.
