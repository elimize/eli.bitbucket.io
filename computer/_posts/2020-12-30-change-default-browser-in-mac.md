---
layout: post
published: true
title: "[맥/Mac] 기본 브라우저 변경하는 방법"
image: /assets/images/mac-browser.jpg
description: >
  MacOs의 기본 브라우저를 변경하는 방법에 대해서 소개해 드립니다.
author: Eli
toc: true
permalink: /computer/change-default-browser-in-mac
tags:
  - macOS
  - 맥OS
  - 기본 브라우저
  - 기본 브라우저 변경
---

맥의 기본 브라우저는 사파리(Safari) 입니다. 사파리는 웹킷 기반으로 만들어진 브라우저로 맥이나 아이폰에 기본적으로 설치 되어 있는 빌트인 앱입니다.

사파리 브라우저는 기본적으로 다른 브라우저들과 비슷하지만 웹 호환성이나 여타 다른 문제들로 인해서 맥 또는 아이폰에서 크롬이나 파이어폭스 등의 다른 브라우저를 설치해서 사용하시는 분들이 많습니다.

사파리 외의 다른 브라우저를 설치해서 사용하는 것은 문제가 없지만 다른 앱, 예를 들어 노트 앱에 있는 하이퍼 링크를 클릭하게 되면 크롬이 아니라 `사파리 앱`을 켜서 이동하는 것을 보실 수 있습니다.

이 글에서는 맥에서 다른 앱에서 링크를 클릭했을 때 기본으로 실행되는 브라우저를 변경하는 방법에 대해서 알아보도록 하겠습니다.

## 기본 브라우저 변경하는 방법

당연하게도 처음 맥을 구입하면 기본 브라우저는 사파리로 설정이 되어 있습니다.

크롬 등의 다른 브라우저가 설치되어 있다고 가정하고,  다음 순서대로 기본 브라우저를 변경하는 방법을 설명해드리도록 하겠습니다.

### 다른 앱에서 링크 클릭

![다른 앱에서 링크 클릭하기](/assets/images/default-browser1.jpg)

위 그림 처럼 노트 앱에서 링크를 클락하면 사파리 브라우저가 켜지고 이동됩니다.

### 설정 앱 실행

![설정 앱 실행하기](/assets/images/default-browser2.jpg)

설정 앱에 진입하기 위해서 하일라이트를 실행하여 `System Preferences`를 검색하여 실행해줍니다.

### 설정 앱에서 검색

![설정 앱 실행하기](/assets/images/default-browser3.jpg)

설정 앱이 실행되고 오른쪽 상단의 검색창에 `set default` 또는 `브라우저` 라고 검색하시면 위 사진처럼 어떤 메뉴에서 설정할 수 있는지 강조해줍니다.

### General 설정

{% include components/google/infeed-ad.html slot=site.data.ad.third.infeed %}

![설정 앱 실행하기](/assets/images/default-browser4.jpg)

강조된 메뉴인 General을 클릭하여 진입합니다.

### 기본 브라우저 변경

![설정 앱 실행하기](/assets/images/default-browser5.jpg)

설정 화면 하단에 `default web browser`에 셀렉트 박스를 클릭하여 기본 브라우저를 선택하실 수 있습니다. 만약에 여러 종류의 브라우저가 설치되어 있으면 브라우저 개수만큼 리스트가 보여집니다.

## 맺음

간단하게 맥에서 기본 브라우저 설정하는 방법에 대해서 알아보았습니다. 혹시 궁금하신 점이나 이상한 점이 있으면 댓글 부탁드리겠습니다.

감사합니다.
